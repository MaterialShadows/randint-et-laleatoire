#!/usr/bin/env python3
# coding: utf8

import fonctions as fAlea
import matplotlib.pyplot as plt
from math import sqrt
from decimal import Decimal as d

def graphique1(nbLance=1000000, nbValeur=20):
    font = {'family': 'sans-serif',
            'color':  'red',
            'weight': 'normal',
            'size': 14,
            }
    axes = plt.gca()
    axes.xaxis.set_ticks(range(21))
    points = fAlea.tirHasard(nbValeur,nbLance)
    coordXPoint = [x+1 for x in list(points.keys())]
    coordYPoint = [y/nbLance for y in list(points.values())]

    plt.title("Probabilité d'apparition en fonction du nombre tiré")
    plt.xlabel('Nombres tirés')
    plt.ylabel('Probabilités')
    plt.plot(coordXPoint,coordYPoint,'ko')
    plt.plot([0,20],[0.05,0.05],'k--')
    plt.plot([0,20],[0.05+(1/sqrt(nbLance)),0.05+(1/sqrt(nbLance))],'r')
    plt.plot([0,20],[0.05-(1/sqrt(nbLance)),0.05-(1/sqrt(nbLance))],'r')
    plt.text(0.5,0.0515,r'$\frac{1}{v}+\frac{1}{\sqrt{n}}$',fontdict=font)
    plt.text(0.5,0.0483,r'$\frac{1}{v}-\frac{1}{\sqrt{n}}$',fontdict=font)
    plt.text(0.5,0.0503,r'$\frac{1}{v}$',fontdict={'color':'black','size':14})
    plt.axis([0,20,0.045,0.055])
    plt.show()
def graphique2():
    nbPaquet = 300000
    coordXPoint=list(range(20))
    coordYPoint=[fAlea.blocMax(nbPaquet) for i in range(20)]

    plt.plot(coordXPoint,coordYPoint,'ko')
    plt.plot([0,20],[0.285,0.285])
    plt.show()
def graphique3(nbCherche=5,nbValeurs=10,maxLance=1000000,pas=1000):
    points = fAlea.frequenceApparition(nbCherche,nbValeurs,maxLance,pas)
    coordXPoint = [x for x in points.keys()]
    coordYPoint = [y for y in points.values()]

    plt.title("Probabilité d'apparition d'un nombre parmis 10 valeurs en fonction du nombre de lancé")
    plt.xlabel("Nombre de lancé")
    plt.ylabel("Probabilité d'apparition d'un nombre")
    plt.plot(coordXPoint,coordYPoint,'ko')
    plt.axis([0,maxLance,0.096,0.105])
    plt.show()
if __name__ == '__main__':
    graphique3()
