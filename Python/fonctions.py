#!/usr/bin/env python
# coding: utf8
from random import randint
from functools import reduce
from operator import mul
from decimal import Decimal as d
import json

def tirHasard(nbValeurs,nbLance):
	'''Fonction remplissant le dictionnaire count par des nombre entre 0 et
	nbValeurs un nombre nbLance de fois'''
	count={}
	for i in range(nbValeurs):
		count[i]=0
	for i in range(nbLance):
		rNb=randint(0,nbValeurs-1)
		count[rNb]+=1
	return count
def frequenceApparition(nbCherche,nbValeurs,maxLance,pas):
	count = {}
	for i in range(1, maxLance, pas):
		count[i] = 0
		for j in range(i):
			if randint(0,nbValeurs-1) == nbCherche:
				count[i] += 1
		count[i]=count[i]/i
	return count
def poker(nbChiffre=100000):
	'''Test du poker'''
	count = {"AAAA":0,"AAAB":0,"AABB":0,"AABC":0,"ABCD":0}
	values = [[randint(0,9),randint(0,9),randint(0,9),randint(0,9)] for i in range(nbChiffre)]
	for paquet in values:
		paquet = [paquet.count(valeurPaquet) for valeurPaquet in paquet]
		paquet = reduce(mul,paquet)
		if paquet == 1:
			count["ABCD"]+=1
		elif paquet == 4:
			count["AABC"]+=1
		elif paquet == 16:
			count["AABB"]+=1
		elif paquet == 27:
			count["AAAB"]+=1
		elif paquet == 256:
			count["AAAA"]+=1
	for config,valeur in count.items():
		count[config] = valeur/nbChiffre
	return count
def blocMax(nbPaquet=5000):
	count = 0
	values = [[randint(1,9),randint(1,9),randint(1,9)] for i in range(nbPaquet)]
	for paquet in values:
		if paquet[1]>paquet[0] and paquet[1]>paquet[2]:
			count+=1
	return count/nbPaquet

def main(nbValeurs,nbLance,nbCherche,maxLance,pas):
	dicoValeurs = tirHasard(nbValeurs,nbLance)
	# dicoFrequences = frequenceApparition(nbCherche,nbValeurs,maxLance,pas)
	save(dicoValeurs, 'hasard', 'json')
	# save(dicoFrequences, 'frequences', 'json', 'csv')

if __name__ == '__main__':
	# main(nbValeurs=10,nbLance=10000,nbCherche=9,maxLance=10000,pas=100)
	# print(blocMax())
	listecount = []
	for i in range(100):
		listecount.append(blocMax(100000))
	print(min(listecount),max(listecount))
	print(sum(listecount)/100)
