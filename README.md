# Code source du TPE : randint() et l'aléatoire

Problématique : La fonction randint() de python génère-t-elle vraiment de l'aléatoire ?

Ce dépot contient le code source Python et LaTeX du *TPE* de **Théo Brun** et **Simon Léonard**

Le code à été écris en Python 3.6 et utilise pyplot de matplotlib pour les graphiques.

[Tableau trello](https://trello.com/b/rZVT5Dhy/tpe)
