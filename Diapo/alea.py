from random import randint
from PIL import Image

img = Image.new('L', (256,256))
px = img.load()

for i in range(img.size[0]):
	for j in range(img.size[1]):
		px[i,j] = (randint(0,1)*255)

img.save('pythonalea.png')