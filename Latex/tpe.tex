\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\usepackage[margin=2cm]{geometry}
\usepackage{amsmath,amssymb,mathrsfs}
\usepackage{fourier,erewhon}
\usepackage{listingsutf8}
\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}
\usepackage{hyperref}
\pagestyle{plain}

\newcommand*\partie[1]{\medbreak\noindent\textbf{#1}\smallskip\par\nobreak}
\newcommand\pardessus[2]{\begin{pmatrix}
#1 \\[1.2pt]
#2
\end{pmatrix}}

%Environement pour mieux centré graphique
%(https://www.developpez.net/forums/d693402/autres-langages/autres-langages/latex/tableaux-graphiques-images-flottants/centrer-image-plus-large-textwidth/)
\makeatletter

\newskip\@bigflushglue \@bigflushglue = -100pt plus 1fil

\def\bigcenter{\trivlist \bigcentering\item\relax}
\def\bigcentering{\let\\\@centercr\rightskip\@bigflushglue%
\leftskip\@bigflushglue
\parindent\z@\parfillskip\z@skip}
\def\endbigcenter{\endtrivlist}

\makeatother
%FIN DE L'ENVIRONEMENT

%PDF Meta Information
\hypersetup{
  pdfauthor = {Simon Léonard},
  pdfkeywords = {Simon, Léonard, randint, aléatoire, générateur, TPE, rapport}
  pdftitle = {randint() et l'aléatoire}
}

\title{La fonction randint() de Python génère-t-elle vraiment de l'aléatoire ?}
\author{Théo \bsc{Brun} et Simon \bsc{Léonard}}
\date{Janvier 2018}

\lstset{
  columns=flexible,
  basicstyle=\ttfamily,
  language=Python,
  commentstyle=\color{gray},
  keywordstyle=\color{RedViolet},
  morekeywords={randint,count},
  stringstyle=\color{RoyalBlue},
  numbers=left,
  numbersep=2pt,
  breaklines=true,
  breakatwhitespace=true,
  extendedchars=true,
  literate={é}{{\'e}}1 {î}{{\^i}}1,
  tabsize=5
}

\csname @addtoreset\endcsname{section}{part} %Réinitialise le compteur des section après chaque partie
\renewcommand{\baselinestretch}{1.2}

\begin{document}
\def\labelitemii{$\bullet$}
\DecimalMathComma
  \maketitle
  % \noindent \textbf{Matière liée} : Mathématique et Science de l'Ingénieur (ou ICN)
  \newpage
  \part{Présentation}
  \section{Pourquoi Python ?}
    Python est un language orienté objet interprété dont la première version date de 1991.
    Nous avons utilisé ce language comme support à nos recherches sur l'aléatoire en informatique
    car il est simple d'utilisation (c'est un language haut niveau, c'est à dire qui se rapproche du language humain
    contrairement aux languages bas niveau qui eux se rapprochent du language machine) et il est très prisé de la communauté scientifique.
    Python est très bien documenté même sans accès à internet et possède un très large éventail de bibliothèques.

  \section{La fonction \emph{randint()}}
    Cette fonction fait partie de la bibliothèque \emph{random} (librairie par défaut) et renvoie un nombre aléatoire compris dans l'intervalle \([x;y]\)
    où \(x\) et \(y\) sont deux entiers choisis. Les fonctions de \emph{random} utilisent le générateur de nombre \emph{Mersenne Twister}
    qui est réputé pour sa qualité et sa rapidité. La période du nombre qu'il utilise est de \(2^{19937}-1\).
    D'autres languages comme PHP et Ruby l'utilisent.

  \part{La théorie}
    \section{L'aléatoire}
      L'aléatoire, synonyme de hasard, peut être défini comme un phénomène imprévisible,
      du simple lancer de dé en passant par les mouvements des particules subatomiques.
      Le hasard est par exemple à l'origine du genre d'un nouveau né, de l'apparition de la vie...
      \medskip

      Une machine ne fait aucun choix aléatoirement. Les nombres aléatoires venant d’une
      machine sont calculés à partir de plusieurs paramètres permettant de se rapprocher
      au mieux du \og vrai \fg\ aléatoire, mais cela n’en est pas : c’est du pseudo-aléatoire.

    \section{Le pseudo-aléatoire}
      Un ordinateur est uniquement capable de réaliser des calculs.
      Ces derniers ne pouvant fournir que des résultats précis, il doit se baser sur
      une \emph{seed} (=graine) la plus hasardeuse possible.
      La \emph{seed} est le premier nombre donné à un algorithme dont découlent toutes les autres valeurs.
      Voyons un exemple avec la suite de Syracus définie ci-après.
      Soit \(N\), la \emph{seed}, strictement supérieur à 0 et \(u\) la suite produite :
      \[u_{n+1}=
      \begin{cases}
        \dfrac{u_n}{2} &\text{si \(u_n\) est pair,}\\
        3u_n+1 &\text{si \(u_n\) est impair.}
      \end{cases}
      \]
      Chaque suite sera différente en fonction \(N\), mais une même valeur donnera une même suite.
      Donc pour une même \emph{seed}, la suite retournée toujours sera la même.

      Si une graine est faite à partir de l'heure, sa période est de 24 heures,
      c'est à dire que le shéma se répète toutes les 24 heures. Ce n'est donc pas un aléatoire suffisant.
      Les \emph{seeds} produites par un phénomène physique sont plus fiables.
      Par exemple :
      \begin{itemize}
        \item le temps que met un électron pour faire le tour d'un atome donné,
        \item les ondes captées par une antenne,
        \item les parasites produits par le vent tapant dans un micro.
      \end{itemize}
      Mais les \emph{seeds} faites à partir de ces phénomènes peuvent être répétées si les mêmes conditions sont réunies.
      Donc on ne fait qu'appeler ça du \og pseudo \fg\ aléatoire.

      Il existe des générateurs qui reposent sur des phénomènes physiques, chaotiques ou quantiques
      dont la qualité se rapproche beaucoup de l'aléatoire. Par exemple, le site \emph{random.org}
      utilise les bruits dans l'atmosphère.

    \section{Tester un générateur}
      Pour tester si un générateur de nombre aléatoire est fiable, il doit être soumis à divers tests
      permettant de valider ou non son efficacité :
      \begin{itemize}
        \item les tests théoriques, consistant à prouver mathématiquement que le
        générateur possède des propriétés acceptables,
        \item les tests empiriques, basés sur des expériences :
          \begin{itemize}
            \item Le test des fréquences consiste à regarder la fréquence d'apparition
            de chaque chiffre.
            \item Le test du poker consiste à rassembler les nombres obtenus par
            paquets de 4 et à comparer les fréquences théoriques et pratiques de chaque cas particulier.
            \item Le test du bloc maximal travaille par bloc de 3 nombres et calcule la probabilité qu'un bloc soit maximal.
            Un bloc est dit maximal si le chiffre du milieu est strictement supérieur aux 2 autres.
          \end{itemize}
      \end{itemize}
      Ici, nous testerons le générateur empiriquement.
  \part{La pratique}
    %TODO : Mettre en évidence le motif que crée Mersenne Twister
    \section{Test des fréquences}
      Le test des fréquences consiste à regarder la probabilité que chaque nombre sorte.
      Ainsi, pour l'aléatoire théorique nous somme censé obtenir une probabilité
      de \(\frac{1}{v}\) où \(v(\in\mathbb{N})\) est le nombre de valeurs possibles.

      %TODO:ADD fréquence stabilisé quand n grandit
      Dans le cas d'un générateur informatique, on admet une marge d'erreur de
      \(\frac{1}{v}\pm\frac{1}{\sqrt{n}}\) avec \(n\) le nombre de tirages.
      Plus \(n\) est grand, meilleure est la précision. Voyons cela avec un exemple :
      \begin{bigcenter}
        \includegraphics[scale=0.78]{NGrandPrecisionAugmente.eps}
      \end{bigcenter}
      On remarque que plus \(n\) augmente, plus les points sont concentrés. Voici le code :
      \begin{lstlisting}
        from random import randint
        def stabilisation(maxLance=1000000,pas=1000,nbValeur=10,nbCherche=5)
          compteur = {} # On définit un dictionnaire (liste organisée par clé/valeur)
        	for nbLance in range(1, maxLance, pas):
        		compteur[nbLance] = 0 # Initialisation du compteur
        		for numeroDeLance in range(nbLance):
        			if randint(0,nbValeurs-1) == nbCherche: # On cherche la probabilité d'apparition d'un nombre de l'intervalle
        				compteur[nbLance] += 1
        		compteur[nbLance] = compteur[nbLance] / nbLance # On passe des effectifs aux probabilités
        	return compteur
      \end{lstlisting}
      Pour l'expérience, le code suivant a été utilisé avec \(v=20\) et \(n=\np{1000000}\) :
      \begin{lstlisting}
        from random import randint

        compteur = {} # Initialisation du dictionnaire
        for valeur in range(v):
          compteur[valeur] = 0 # Initialisation de la liste
        for i in range(n):
          rNb = randint(1,v) # On prend un nombre entre 1 et 20
          compteur[rNb] += 1 # Pour la clé correspondante, on ajoute 1
        print(compteur)
      \end{lstlisting}
      Après avoir divisé chaque nombre obtenu par \(v\), on obtient les probabilités suivantes :
      \begin{center}\includegraphics[scale=0.8]{frequenceNbTire.eps}\end{center}
      D'après le graphique, nous pouvons conclure que le test est réussi car aucune valeur ne sort de la marge d'erreur (représentée ici par les droites rouges).
    \section{Test du Poker}
      Pour ce test-ci, nous tirons \(n\) mains (ou paquets) de 4 chiffres entre 0 et 9.
      Chacune des \(10^4\) probabilités entre dans une des 5 configurations possibles ci-après.
      Nous devons connaître les probabilités pour chacune d'entre elles :
      \partie{Configuration ABCD (tous les chiffres sont différents)}
        Pour trouver la probabilité théorique quand les quatre chiffres sont différents :
        \[P(\text{"ABCD"})=\frac{10\times9\times8\times7}{10^4}=0,504\]

        Car pour le premier chiffre il y a 10 possibilités, pour le second 9 et ainsi de suite divisé par le nombre total de possibilités.
      \partie{Configuration AABC (une paire)}
        Pour la configuration BAAC, on a \(10\times9\times8\times1\) façons de créer la paire que l'on multiplie par le nombre de façons de placer la paire parmi les 4 places possibles :
        \[P(\text{"AABC"})=\pardessus{4}{2}\frac{10\times9\times8}{10^4}=0,432\]
      \partie{Configuration AABB (deux paires)}
        On a 10 façons de choisir le premier numéro, puis 9 façons de choisir le second (qui doit être différent) soit \(10\times9=90\) choix à multiplier par le nombre de façons de placer ces numéros parmi les 4 places possibles :
        \[P(\text{"AABB"})=\frac{1}{2}\pardessus{4}{2}\frac{10\times9}{10^4}=0,027\]
      \partie{Configuration AAAB (trois chiffres identiques)}
        Cette fois, on a \(10\times9\) possibilités à multiplier par le nombre de façons de placer les trois chiffres parmi les 4 places possibles :
        \[P(\text{"AAAB"})=\pardessus{4}{3}\frac{10\times9}{10^4}=0,036\]
      \partie{Configuration AAAA (quatre chiffres identiques)}
        Et pour cette dernière configuration, nous avons 10 chiffres possibles divisés par le nombre de possibilités total :
        \[P(\text{"AAAA"})=\frac{10}{10^4}=0,001\]

      Pour résumer tout cela :
      \begin{center}
        \begin{tabular}{|c|c|c|c|c|c|}
          \hline
          Configurations & ABCD & AABC & AABB & AAAB & AAAA\\
          \hline
          Exemples & 2419 & 3893 & 7337 & 7222 & 6666\\
          \hline
          Probabilités théoriques & 0,504 & 0,432 & 0,027 & 0,036 & 0,001\\
          \hline
        \end{tabular}
      \end{center}

      Mais une machine, contrairement à un humain, doit passer par une autre étape
      pour savoir quel groupe de 4 appartient à quelle configuration.
      Un programme balaye donc chaque groupe et remplace chaque valeur du groupe
      par le nombre de fois où elle apparaît dans le groupe.
      Un exemple sera plus parlant : [1,3,2,3] devient [1,2,1,2], [7,7,7,5] devient [3,3,3,1]...
      Il suffit ensuite de multiplier chaque nombre des listes obtenues entre eux
      et cela donne un seul nombre par configuration :
      \begin{itemize}
        \item 1 pour ABDC,
        \item 4 pour AABC,
        \item 16 pour AABB,
        \item 27 pour AAAB
        \item 256 pour AAAA.
      \end{itemize}
      Voici le code utilisé avec \(n\) = \np{1000000} soit \(\np{1000000}\times4=\np{4000000}\) de chiffres:

      \begin{lstlisting}
        from random import randint
        from functools import reduce
        from operator import mul

        compteur = {"AAAA":0,"AAAB":0,"AABB":0,"AABC":0,"ABCD":0} # On initialise le dictionnaire
        # On crée une liste de 1000000 mains de 4 chiffres
        listeMains = [[randint(0,9),randint(0,9),randint(0,9),randint(0,9)] for i in range(1000000)]
        for main in listeMains:
          # On remplace chaque valeur par le nombre de fois que cette valeur existe dans la main
          typeConfig = [main.count(carte) for carte in main]
          typeConfig = reduce(mul, typeConfig) # On multiplie les quatre nombres entre eux
          # On traite chaque cas
          if typeConfig == 1:
            compteur["ABCD"]+=1
          elif typeConfig == 4:
            compteur["AABC"]+=1
          elif typeConfig == 16:
            compteur["AABB"]+=1
          elif typeConfig == 27:
            compteur["AAAB"]+=1
          elif typeConfig == 256:
            compteur["AAAA"]+=1
        # Enfin, on passe des effectifs aux probabilités
        for config,valeur in compteur.items():
      		compteur[config] = valeur/nbChiffre
        print(compteur)
      \end{lstlisting}

      Voici les résulats obtenus :
      %{'AAAA': 0.00108, 'AAAB': 0.03686, 'AABB': 0.02687, 'AABC': 0.43141, 'ABCD': 0.50378}

      \begin{center}
        \begin{tabular}{|c|c|c|c|c|c|}
          \hline
          Configuration & ABCD & AABC & AABB & AAAB & AAAA\\
          \hline
          Probabilités obtenues & 0,50378 & 0,43141 & 0,02687 & 0,03686 & 0,00108\\
          \hline
          Probabilités théoriques & 0,504 & 0,432 & 0,027 & 0,036 & 0,001\\
          \hline
        \end{tabular}
      \end{center}

      Les probabilités obtenues sont suffisament proches des probabilités théoriques
      pour considérer que le test est réussi.

    \section{Test du bloc maximal}
      Ce test travaille par paquets de 3 et regarde si le chiffre du milieu est strictement supérieur aux deux autres.
      Si c'est le cas, alors le paquet est dit maximal. Il a été démontré que la
      probabilité qu'un paquet soit maximal est de 0,285.
      Nous avons choisi de générer \np{100000} paquets et de les passer dans l'algorithme
      puis de réitérer l'opération 100 fois pour avoir un encadrement :

      \begin{lstlisting}
        # Fonction test du bloc maximal
        def blocMax(nbPaquet):
          compteur = 0 # Initialisation du compteur
          # On crée nbPaquet paquets
          values = [[randint(1,9),randint(1,9),randint(1,9)] for i in range(nbPaquet)]
          for paquet in values:
            if paquet[1]>paquet[0] and paquet[1]>paquet[2]: # Si le paquet est maximal
              compteur+=1 # On indente le compteur de 1
          return compteur/nbPaquet # On retourne la probabilité obtenue

        # On appele plusieurs fois la fonction
        valeursRetourne = list()
        for i in range(100):
      		valeursRetourne.append(blocMax(100000))
        print(min(listecount),max(listecount)) # Puis on affiche les valeurs maximale et minimale
      \end{lstlisting}

      Empiriquement, nous avons obtenu des valeurs entre 0,27693 et 0,28346.
      Elles sont donc assez proches de la valeur théoriques mais nous n'avons
      toutefois pas pu atteindre 0,285. Néanmoins, nous considérons que les valeurs sont suffisament
      proches pour valider le test.
  \part{Conclusion}
    Nous avons d'abord testé empiriquement la fréquence d'apparition des nombres de l'intervalle \(\llbracket1;20\rrbracket\).
    Avec un générateur \og vraiment \fg\ aléatoire et pour 20 valeurs, nous avons
    vu que la probabilité est de \(\frac{1}{20}=0,05\). Dans le cadre d'un générateur pseudo-aléatoire,
    on admet un marge d'erreur de \(\pm \frac{1}{\sqrt{n}}\) avec \(n\) le nombre de tirages.
    \emph{randint()} a passé cette étape avec succès.

    Ensuite, le test du poker a révélé que les probabilités obtenues étaient proches des théoriques
    et que la probabilité de chaque configurations était de même ordre : ABCD>AABC>AAAB>AABB>AAAA.
    Ce test-ci a donc aussi été validé.

    Et pour finir, le test du bloc maximal devait, pour un générateur parfaitement aléatoire,
    fournir une probabilité de 0,285. Nous avons obtenu un résultat maximal de 0,283, suffisant pour un  générateur pseudo-aléatoire.

    L'aléatoire fourni par Python est de qualité suffisante dans la plupart des applications.
    En revanche, pour une utilisation précise il vaudra mieux se tourner vers des solutions
    fournissant de l'aléatoire basé sur des phénomènes physiques.
  \part{Concernant le projet}
    \section{Organisation}
      Théo s'est occupé pour la majeure partie des recherches concernant l'aléatoire,
      donc plutôt de la partie appelée précédemment \og théorique \fg. Quant à Simon,
      il s'est surtout occupé de la partie appelée précédemment \og pratique \fg\ et de
      la conception du présent document.
      \smallskip

      Notre carnet de bord ainsi que notre état d'avancement ont été gérés grâce au service \emph{Trello.com}
      qui fournit un système de tableau composé de cartes déplaçables.
      Le présent document a été rédigé en \LaTeX, un language de description de document.
      Pour suivre l'avancement du code source (Python et \LaTeX), le système de
      \emph{versioning} \og Git \fg\ a été utilisé avec la plateforme Bitbucket.

      \begin{center}
        \begin{tabular}{cc}
          \includegraphics[width=110px]{qrBitbucket.png} & \includegraphics[width=113px]{qrTrello.png}\\
          Code source & Organisation
        \end{tabular}
      \end{center}
      \section{Sitiographie}
        \begin{itemize}
          \item http://math.univ-lyon1.fr/~jberard/genunif-www.pdf
          \item https://www.python.org/about/success/usa/
          \item http://www.journaldunet.com/developpeur/tutoriel/theo/060324-generer-le-hasard.shtml
          \item https://www.youtube.com/watch?v=TfoZTQ9sU3Y
          \item http://www.tangentex.com/GenerateurAleatoire.htm
          \item https://univers-ti-nspire.fr/files/pdf/16-genealea-TNS21.pdf
          \item https://lmb.univ-fcomte.fr/IMG/pdf/gen.pdf
        \end{itemize}
\end{document}
